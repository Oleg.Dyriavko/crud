package com.company;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        br.close();

        if (args.length > 0 && args[0].equals("-c")) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            ArrayList<String> arrayList = new ArrayList<>();
            String line;
            int newId = 0;
            while ((line = bufferedReader.readLine()) != null)
                if (Integer.parseInt(line.substring(0, 8).trim()) > newId)
                    newId = Integer.parseInt(line.substring(0, 8).trim()) + 1;

            arrayList.add(line);
            bufferedReader.close();

            FileWriter fileWriter = new FileWriter(fileName, true);
            fileWriter.write(String.format("\n%-8.8s%-30.30s%-8.8s%-4.4s", newId, args[1], args[2], args[3]));
            fileWriter.flush();
            fileWriter.close();
        }

        if (args.length > 0 && args[0].equals("-u")) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String line;
            ArrayList<String> list = new ArrayList<>();
            int newId = Integer.parseInt(args[1]);
            while ((line = bufferedReader.readLine()) != null)
                if (Integer.parseInt(line.substring(0, 8).trim()) != newId) {
                    list.add(line);
                } else {
                    list.add(String.format("%-8s%-30s%-8s%-4s", newId, args[2], args[3], args[4]));
                }

            bufferedReader.close();

            FileWriter fileWriter = new FileWriter(fileName, true);

            for (String s : list) {
                fileWriter.write(s);
                fileWriter.write("\n");
            }
            fileWriter.flush();
            fileWriter.close();

        }

        if (args.length > 0 && args[0].equals("-d")) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            ArrayList<String> list = new ArrayList<>();
            String line;
            int newId = Integer.parseInt(args[1]);
            while ((line = bufferedReader.readLine()) != null)
                if (Integer.parseInt(line.substring(0, 8).trim()) != newId)
                    list.add(line);
            bufferedReader.close();

            FileWriter fileWriter = new FileWriter(fileName, true);
            for (String s : list) {
                fileWriter.write(s);
                fileWriter.write("\n");
            }

            fileWriter.flush();
            fileWriter.close();
        }
    }
}
